# frozen_string_literal: true

control 'mounts-ng-fstab-contents' do
  title 'it should contain correct mounts'

  describe file('/etc/fstab') do
    it { should be_file }
    its('content') { should match '\/srv\s+\/opt\/srv\s+none\s+bind\s+0\s+0' }
    its('content') do
      should match '\/srv\/nextcloud\/auser\s+'
      '\/home\/auser\/nextcloud\s+none\s+bind\s+0\s+0'
    end
    its('content') do
      should match '\/srv\/nextcloud\/auser\/documents\s+'
      '\/home\/auser\/documents\s+none\s+bind\s+0\s+0'
    end
  end
end

control 'mounts-ng-config-bing-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'mounts-ng-bin-dir-managed' do
  title 'it should exist'

  describe directory('/home/auser/bin') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'mounts-ng-desktop-dir-managed' do
  title 'it should not exist'

  describe directory('/home/auser/Desktop') do
    it { should_not exist }
  end
end

control 'mounts-ng-downloads-symlink-managed' do
  title 'it should be configured correctly'

  describe file('/home/auser/Downloads') do
    it { should exist }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('link_path') { should eq '/home/auser/downloads' }
  end
end
