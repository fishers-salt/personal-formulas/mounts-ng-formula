# -*- coding: utf-8 -*-
# vim: ft=yaml
---
nfs:
  # Global settings:
  mkmnt: false
  mount_opts: noauto,ro
  persist_unmount: true
  persist_mount: false
  enabled: true

  # Server settings
  server:
    exports:
      /home/kitchen: "*(rw,sync,no_subtree_check)"
