# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as mounts_ng with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set systemd_network_mounts = mounts_ng.get('systemd-network-automount', {}) %}
{%- for systemd_mount, data in systemd_network_mounts.items() %}
  {%- set name = data.name %}
  {%- set mountpoint = data.mountpoint %}
  {%- set server = data.server %}
  {%- set share_location = data.share_location %}
  {%- if data.user is defined %}
    {%- set username = data.user %}
  {%- else %}
    {%- set username = 'root' %}
  {%- endif %}
  {%- if data.group is defined %}
    {%- set group = data.group %}
  {%- else %}
    {%- set group = 'root' %}
  {%- endif %}
  {%- if data.dir_mode is defined %}
    {%- set dir_mode = data.dir_mode %}
  {%- else %}
    {%- set dir_mode = '0755' %}
  {%- endif %}
  {%- if data.share_type is defined %}
    {%- set share_type = data.share_type %}
  {%- else %}
    {%- set share_type = 'nfs' %}
  {%- endif %}
  {%- if data.create_target is defined %}
    {%- set create_target = data.create_target %}
  {%- else %}
    {% set create_target = True %}
  {%- endif %}

{%- if create_target %}

mounts-ng-config-mounts-systemd-{{ systemd_mount }}-target-point-managed:
  file.directory:
    - name: {{ name }}
    - user: {{ username }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - makedirs: True
    {%- if username == 'root' and group == 'root' %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %}
    {%- endif %}
{%- endif %}

  {# <server IP address>:<NFS share> <local mount location> nfs nofail,x-systemd.automount,x-systemd.requires=network-online.target,x-systemd.device-timeout=10s
 #}
mounts-ng-config-mounts-systemd-{{ systemd_mount }}-mount-point-managed:
  mount.mounted:
    - name: {{ name }}
    - device: {{ server }}:{{ share_location }}
    - fstype: {{ share_type }}
    - opts:
      - nofail
      - noauto
      - x-systemd.automount
      - x-systemd.mount-timeout=30
      - x-systemd.idle-timeout=5min
      - _netdev
    - dump: 0
    - pass_num: 0
    - persist: True
    - mkmnt: False
    {%- if create_target %}
    - require:
      - mounts-ng-config-mounts-systemd-{{ systemd_mount }}-target-point-managed
    {%- endif %}

{%- endfor %}
