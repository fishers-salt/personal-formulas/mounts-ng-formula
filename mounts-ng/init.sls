# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .bind
  - .directories
  - .absent
  - .symlinks
  - .network_mounts
