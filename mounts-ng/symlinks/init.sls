# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as mounts_ng with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set symlinks = mounts_ng.get('symlinks', {}) %}

{%- for symlink, data in symlinks.items() %}
  {%- set name = data.name %}
  {%- set target = data.target %}
  {%- if data.user is defined %}
    {%- set username = data.user %}
  {%- else %}
    {%- set username = 'root' %}
  {%- endif %}
  {%- if data.group is defined %}
    {%- set group = data.group %}
  {%- else %}
    {%- set group = 'root' %}
  {%- endif %}
  {%- if data.dir_mode is defined %}
    {%- set dir_mode = data.dir_mode %}
  {%- else %}
    {%- set dir_mode = '0755' %}
  {%- endif %}
  {%- if data.force is defined %}
    {%- set force = data.force %}
  {%- else %}
    {%- set force = False %}
  {%- endif %}
  {%- if create_target is defined %}
    {%- set create_target = data.create_target %}
  {%- else %}
    {%- set create_target = True %}
  {%- endif %}

{%- if create_target %}
mounts-ng-config-symlinks-{{ symlink }}-target-managed:
  file.directory:
    - name: {{ target }}
    - user: {{ username }}
    - makedirs: True
    - dir_mode: {{ dir_mode }}
    {%- if username == 'root' and group == 'root' %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %}
    {%- endif %}
{%- endif %}

mounts-ng-config-symlinks-{{ symlink }}-managed:
  file.symlink:
    - name: {{ name }}
    - target: {{ target }}
    - force: {{ force }}
    - user: {{ username }}
    - group: {{ group }}
     {%- if username == 'root' and group == 'root' and not create_target %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %}
      {%- if create_target %}
      - mounts-ng-config-symlinks-{{ symlink }}-target-managed
      {%- endif %}
    {%- endif %}

{%- endfor %} {# for symlink, data in symlinks.items() #}

{% if salt['pillar.get']('mounts-ng-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
  {%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_mounts-ng', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
  {%- set user_group = user.prime_group.name -%}
{%- else -%}
  {%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

mounts-ng-config-symlinks-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

mounts-ng-config-symlinks-group-{{ user_group }}-present:
  group.present:
    - name: {{ user_group }}

{%- set mounts_ng_user = user.get('mounts-ng', {}) %}
{%- if mounts_ng_user != {} %}
{%- set symlinks = mounts_ng_user.get('symlinks', {}) %}
{%- for symlink, data in symlinks.items() %}
  {%- set symlink_name = data.name %}
  {%- set target = data.target %}
  {%- if data.user is defined %}
    {%- set username = data.user %}
  {%- else %}
    {%- set username = name %}
  {%- endif %}
  {%- if data.group is defined %}
    {%- set group = data.group %}
  {%- else %}
    {%- set group = user_group %}
  {%- endif %}
  {%- if data.dir_mode is defined %}
    {%- set dir_mode = data.dir_mode %}
  {%- else %}
    {%- set dir_mode = '0755' %}
  {%- endif %}
  {%- if data.force is defined %}
    {%- set force = data.force %}
  {%- else %}
    {%- set force = False %}
  {%- endif %}
  {%- if create_target is defined %}
    {%- set create_target = data.create_target %}
  {%- else %}
    {%- set create_target = True %}
  {%- endif %}

{%- if create_target %}
mounts-ng-config-symlinks-{{ symlink }}-{{ name }}-target-managed:
  file.directory:
    - name: {{ target }}
    - user: {{ username }}
    - group: {{ group }}
    - makedirs: True
    - dir_mode: {{ dir_mode }}
    {%- if username == 'root' and group == 'root' %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %} {# username != root #}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %} {# group != root #}
    {%- endif %} {# username and group != root #}
{%- endif %} {# if create_target #}

mounts-ng-config-symlinks-{{ symlink }}-{{ name }}-symlink-managed:
  file.symlink:
    - name: {{ symlink_name }}
    - target: {{ target }}
    - force: {{ force }}
    - user: {{ username }}
    - group: {{ group }}
     {%- if username == 'root' and group == 'root' and not create_target %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %}
      {%- if create_target %}
      - mounts-ng-config-symlinks-{{ symlink }}-{{ name }}-target-managed
      {%- endif %}
    {%- endif %}

{% endfor %} {# for symlinks_mount, data in symlinks_mounts.items() #}
{% endif %}  {# if mounts_ng_user != {} #}
{% endif %}  {# if manage #}
{% endfor %} {# for name, user in pillar.get(users) #}
{% endif %} {# if use_users_formula #}
