# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as mounts_ng with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set directories = mounts_ng.get('directories', {}) %}

{%- for directory, data in directories.items() %}
  {%- set name = data.name %}
  {%- if data.user is defined %}
    {%- set username = data.user %}
  {%- else %}
    {%- set username = 'root' %}
  {%- endif %}
  {%- if data.group is defined %}
    {%- set group = data.group %}
  {%- else %}
    {%- set group = 'root' %}
  {%- endif %}
  {%- if data.dir_mode is defined %}
    {%- set dir_mode = data.dir_mode %}
  {%- else %}
    {%- set dir_mode = '0755' %}
  {%- endif %}
mounts-ng-config-directories-{{ directory }}-managed:
  file.directory:
    - name: {{ name }}
    - user: {{ username }}
    - group: {{ group }}
    - makedirs: True
    - dir_mode: {{ dir_mode }}
    {%- if username == 'root' and group == 'root' %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %}
    {%- endif %}
{%- endfor %}

{% if salt['pillar.get']('mounts-ng-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_mounts-ng', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

{%- set mounts_ng_user = user.get('mounts-ng', {}) %}
{%- if mounts_ng_user != {} %}
{%- set directories = mounts_ng_user.get('directories', {}) %}
{%- for directory, data in directories.items() %}
  {%- set directory_name = data.name %}
  {%- if data.user is defined %}
    {%- set username = data.user %}
  {%- else %}
    {%- set username = name %}
  {%- endif %}
  {%- if data.group is defined %}
    {%- set group = data.group %}
  {%- else %}
    {%- set group = user_group %}
  {%- endif %}
  {%- if data.dir_mode is defined %}
    {%- set dir_mode = data.dir_mode %}
  {%- else %}
    {%- set dir_mode = '0755' %}
  {%- endif %}

mounts-ng-config-directories-{{ directory }}-{{ username }}-managed:
  file.directory:
    - name: {{ directory_name }}
    - user: {{ username }}
    - group: {{ group }}
    - makedirs: True
    - dir_mode: {{ dir_mode }}
    {%- if username == 'root' and group == 'root' %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %}
    {%- endif %}

{% endfor %}
{% endif %}
{% endif %}
{% endfor %}
{% endif %}
