# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as mounts_ng with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}


{%- if mounts_ng.debug is defined %}
{%- if mounts_ng.debug %}
mounts-ng-config-debug-mountsdata-test-file-managed:
  file.managed:
    - name: /tmp/mountsdata
    - contents: {{ mounts_ng }}

{%- set bind_mounts = mounts_ng.get('bind', {}) %}
mounts-ng-config-file-bind-mounts-test-file-managed:
  file.managed:
    - name: /tmp/binddata
    - contents: {{ bind_mounts }}

{%- set directories = mounts_ng.get('directories', {}) %}
mounts-ng-config-file-directory-mounts-test-file-managed:
  file.managed:
    - name: /tmp/directorydata
    - contents: {{ directories }}

{%- set absents = mounts_ng.get('absent', {}) %}
mounts-ng-config-file-absent-mounts-test-file-managed:
  file.managed:
    - name: /tmp/absentdata
    - contents: {{ absents }}

{%- set symlinks = mounts_ng.get('symlinks', {}) %}
mounts-ng-config-file-symlink-mounts-test-file-managed:
  file.managed:
    - name: /tmp/symlinkdata
    - contents: {{ symlinks }}
{#

{%- set systemd_network_mounts = mounts_ng.get('systemd-network-automount', {}) %}
mounts-ng-config-debug-systemd-test-file-managed:
  file.managed:
    - name: /tmp/systemd-mounts
    - contents: {{ systemd_network_mounts }}
#}
mounts-ng-config-file-pillar-mounts-test-file-managed:
  file.managed:
    - name: /tmp/pillardata
    - contents_pillar: mounts-ng

{%- set tempdata = salt['pillar.get']('mounts-ng') %}
mounts-ng-config-file-pillar-direct-mounts-test-file-managed:
  file.managed:
    - name: /tmp/pillardirectdata
    - contents: {{ tempdata }}
{%- endif %}
{%- endif %}
