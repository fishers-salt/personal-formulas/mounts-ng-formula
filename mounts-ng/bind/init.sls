# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as mounts_ng with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set bind_mounts = mounts_ng.get('bind', {}) %}

{%- for bind_mount, data in bind_mounts.items() %}
  {%- set name = data.name %}
  {%- set target = data.target %}
  {%- if create_target is defined %}
    {%- set create_target = data.create_target %}
  {%- else %}
    {%- set create_target = True %}
  {%- endif %}
  {%- if data.user is defined %}
    {%- set username = data.user %}
  {%- else %}
    {%- set username = 'root' %}
  {%- endif %}
  {%- if data.group is defined %}
    {%- set group = data.group %}
  {%- else %}
    {%- set group = 'root' %}
  {%- endif %}
  {%- if data.dir_mode is defined %}
    {%- set dir_mode = data.dir_mode %}
  {%- else %}
    {%- set dir_mode = '0755' %}
  {%- endif %}

mounts-ng-config-bind-{{ bind_mount }}-mount-point-managed:
  file.directory:
    - name: {{ name }}
    - user: {{ username }}
    - group: {{ group }}
    - makedirs: True
    - dir_mode: {{ dir_mode }}
    {%- if username == 'root' and group == 'root' %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %}
    {%- endif %}

{%- if create_target %}
mounts-ng-config-bind-{{ bind_mount }}-target-point-managed:
  file.directory:
    - name: {{ target }}
    - user: {{ username }}
    - group: {{ group }}
    - makedirs: True
    - dir_mode: {{ dir_mode }}
    {%- if username == 'root' and group == 'root' %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %}
    {%- endif %}
{%- endif %}

mounts-ng-config-bind-{{ bind_mount }}-mount-managed:
  mount.mounted:
    - name: {{ name }}
    - device: {{ target }}
    - fstype: none
    - opts: bind
    - dump: 0
    - pass_num: 0
    - persist: True
    - mkmnt: False
    - require:
      - mounts-ng-config-bind-{{ bind_mount }}-mount-point-managed
      {%- if create_target %}
      - mounts-ng-config-bind-{{ bind_mount }}-target-point-managed
      {%- endif %}
{%- endfor %}

{% if salt['pillar.get']('mounts-ng-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_mounts-ng', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

mounts-ng-config-bind-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

mounts-ng-config-bind-group-{{ user_group }}-present:
  group.present:
    - name: {{ user_group }}

{%- set mounts_ng_user = user.get('mounts-ng', {}) %}
{%- if mounts_ng_user != {} %}
{%- set bind_mounts = mounts_ng_user.get('bind', {}) %}
{%- for bind_mount, data in bind_mounts.items() %}
  {%- set mount_name = data.name %}
  {%- set target = data.target %}
  {%- if create_target is defined %}
    {%- set create_target = data.create_target %}
  {%- else %}
    {%- set create_target = True %}
  {%- endif %}
  {%- if data.user is defined %}
    {%- set username = data.user %}
  {%- else %}
    {%- set username = name %}
  {%- endif %}
  {%- if data.group is defined %}
    {%- set group = data.group %}
  {%- else %}
    {%- set group = user_group %}
  {%- endif %}
  {%- if data.dir_mode is defined %}
    {%- set dir_mode = data.dir_mode %}
  {%- else %}
    {%- set dir_mode = '0755' %}
  {%- endif %}

mounts-ng-config-bind-{{ bind_mount }}-{{ name }}-mount-point-managed:
  file.directory:
    - name: {{ mount_name }}
    - user: {{ username }}
    - group: {{ group }}
    - makedirs: True
    - dir_mode: {{ dir_mode }}
    {%- if username == 'root' and group == 'root' %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %} {# username != root #}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %} {# group != root #}
    {%- endif %} {# username and group != root #}

{%- if create_target %}
mounts-ng-config-bind-{{ bind_mount }}-{{ name }}-target-point-managed:
  file.directory:
    - name: {{ target }}
    - user: {{ username }}
    - group: {{ group }}
    - makedirs: True
    - dir_mode: {{ dir_mode }}
    {%- if username == 'root' and group == 'root' %}
    {%- else %}
    - require:
      {%- if username != 'root' %}
      - user: {{ username }}
      {%- endif %} {# username != root #}
      {%- if group != 'root' %}
      - group: {{ group }}
      {%- endif %} {# group != root #}
    {%- endif %} {# username and group != root #}
{%- endif %} {# if create_target #}

mounts-ng-config-bind-{{ bind_mount }}-{{ name }}-mount-managed:
  mount.mounted:
    - name: {{ mount_name }}
    - device: {{ target }}
    - fstype: none
    - opts: bind
    - dump: 0
    - pass_num: 0
    - persist: True
    - mkmnt: False
    - require:
      - mounts-ng-config-bind-{{ bind_mount }}-{{ name }}-mount-point-managed
      {%- if create_target %}
      - mounts-ng-config-bind-{{ bind_mount }}-{{ name }}-target-point-managed
      {%- endif %} {# if create_target #}

{% endfor %} {# for bind_mount, data in bind_mounts.items() #}
{% endif %}  {# if mounts_ng_user != {} #}
{% endif %}  {# if manage #}
{% endfor %} {# for name, user in pillar.get(users) #}
{% endif %} {# if use_users_formula #}
