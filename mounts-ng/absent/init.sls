# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as mounts_ng with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set directories = mounts_ng.get('absent', {}) %}

{%- for directory, data in directories.items() %}
  {%- set name = data.name %}
mounts-ng-config-directories-{{ directory }}-absent:
  file.absent:
    - name: {{ name }}
{%- endfor %}

{% if salt['pillar.get']('mounts-ng-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_mounts-ng', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

{%- set mounts_ng_user = user.get('mounts-ng', {}) %}
{%- if mounts_ng_user != {} %}
{%- set directories = mounts_ng_user.get('absent', {}) %}
{%- for directory, data in directories.items() %}
  {%- set directory_name = data.name %}

mounts-ng-config-directories-{{ directory }}-{{ name }}-absent:
  file.absent:
    - name: {{ directory_name }}

{% endfor %}
{% endif %}
{% endif %}
{% endfor %}
{% endif %}
