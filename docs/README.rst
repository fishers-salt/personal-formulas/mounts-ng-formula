.. _readme:

mounts-ng-formula
=================

|img_travis| |img_sr| |img_pc|

.. |img_travis| image:: https://travis-ci.com/saltstack-formulas/mounts-ng-formula.svg?branch=master
   :alt: Travis CI Build Status
   :scale: 100%
   :target: https://travis-ci.com/saltstack-formulas/mounts-ng-formula
.. |img_sr| image:: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
   :alt: Semantic Release
   :scale: 100%
   :target: https://github.com/semantic-release/semantic-release
.. |img_pc| image:: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
   :alt: pre-commit
   :scale: 100%
   :target: https://github.com/pre-commit/pre-commit

Install and configure mounts-ng

.. contents:: **Table of Contents**
   :depth: 1

General notes
-------------

See the full `SaltStack Formulas installation and usage instructions
<https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

If you are interested in writing or contributing to formulas, please pay attention to the `Writing Formula Section
<https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#writing-formulas>`_.

If you want to use this formula, please pay attention to the ``FORMULA`` file and/or ``git tag``,
which contains the currently released version. This formula is versioned according to `Semantic Versioning <http://semver.org/>`_.

See `Formula Versioning Section <https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#versioning>`_ for more details.

If you need (non-default) configuration, please refer to:

- `how to configure the formula with map.jinja <map.jinja.rst>`_
- the ``pillar.example`` file
- the `Special notes`_ section

Contributing to this repo
-------------------------

Commit messages
^^^^^^^^^^^^^^^

**Commit message formatting is significant!!**

Please see `How to contribute <https://github.com/saltstack-formulas/.github/blob/master/CONTRIBUTING.rst>`_ for more details.

pre-commit
^^^^^^^^^^

`pre-commit <https://pre-commit.com/>`_ is configured for this formula, which you may optionally use to ease the steps involved in submitting your changes.
First install  the ``pre-commit`` package manager using the appropriate `method <https://pre-commit.com/#installation>`_, then run ``bin/install-hooks`` and
now ``pre-commit`` will run automatically on each ``git commit``. ::

  $ bin/install-hooks
  pre-commit installed at .git/hooks/pre-commit
  pre-commit installed at .git/hooks/commit-msg

Special notes
-------------

For a comfortable workstation, there are many directory hierarchies that are
required. These are tricky to wrap into a single state, but this is my attempt.

This formula creates bind mounts, creates directories, removes unneeded directories,
creates symlinks, and adds network mounted directories in that order.

Each has a separate state that can be called by itself if the order needs to be
adjusted.

To create these mounts/directories, there are two places they can be added.
Either will do, but you may prefer one over the other depending on your setup. 

1. As a top-level object in pillar called `mounts-ng`.
2. As a object inside a user's pillar (see `users formula
   <https://github.com/saltstack-formulas/users-formula>`_) called
   `users:username:mounts-ng`.

Both methods are demonstrated in `pillar.example,` and there is a disabled
systemd nfs mount-on-demand example as well. It's disabled because any docker
testing requires the host to have the nfs-server kernel module enabled. You may
use either method, or a mixture of both. The mounts will be created in that
order too (top level, then user specific), so you may be able to use this fact
to order the mount creation a bit.

Sadly, this formula does not contain a `clean` state because of the danger of
severe data loss (and a lack of trust in myself).


Available states
----------------

.. contents::
   :local:

``mounts-ng``
^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This creates all configured bind mounts, directories, symlinks, and remote
mounts, and removes any configured directories.

``mounts-ng.bind``
^^^^^^^^^^^^^^^^^^

Remounts parts of the filesystem hierarchy somewhere else.

``mounts-ng.directories``
^^^^^^^^^^^^^^^^^^^^^^^^^

``mounts-ng.absent``
^^^^^^^^^^^^^^^^^^^^

``mounts-ng.symlinks``
^^^^^^^^^^^^^^^^^^^^^^

``mounts.ng.network_mounts``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Testing
-------

Linux testing is done with ``kitchen-salt``.

Requirements
^^^^^^^^^^^^

* Ruby
* Docker

.. code-block:: bash

   $ gem install bundler
   $ bundle install
   $ bin/kitchen test [platform]

Where ``[platform]`` is the platform name defined in ``kitchen.yml``,
e.g. ``debian-9-2019-2-py3``.

``bin/kitchen converge``
^^^^^^^^^^^^^^^^^^^^^^^^

Creates the docker instance and runs the ``mounts-ng`` main state, ready for testing.

``bin/kitchen verify``
^^^^^^^^^^^^^^^^^^^^^^

Runs the ``inspec`` tests on the actual instance.

``bin/kitchen destroy``
^^^^^^^^^^^^^^^^^^^^^^^

Removes the docker instance.

``bin/kitchen test``
^^^^^^^^^^^^^^^^^^^^

Runs all of the stages above in one go: i.e. ``destroy`` + ``converge`` + ``verify`` + ``destroy``.

``bin/kitchen login``
^^^^^^^^^^^^^^^^^^^^^

Gives you SSH access to the instance for manual testing.
